package nocca

import (
	"fmt"
	"testing"
)

func TestBoard_Evaluate(t *testing.T) {
	tests := []struct {
		width, height int
		want          int
	}{
		{5, 6, 0},
		{2, 2, 0},
	}
	for _, test := range tests {
		t.Run(fmt.Sprintf("w=%d h=%d", test.width, test.height), func(t *testing.T) {
			b := NewBoard(test.width, test.height)
			b.Setup()
			if got := b.Evaluate(); got != test.want {
				t.Fatalf("%d != %d\n", got, test.want)
			}
		})
	}
}

func TestBoard_Candidates(t *testing.T) {
	tests := []struct {
		width, height int
		want          int
	}{
		{2, 2, 6},
		{5, 6, 21},
	}

	for _, test := range tests {
		b := NewBoard(test.width, test.height)
		b.Setup()
		if got := b.Candidates(WHITE); len(got) != test.want {
			t.Errorf("len(b.Candiates()) = %d\n", len(got))
			t.Fatalf("got = %v", got)
		}
	}
}

func ExampleBoard() {
	b := NewBoard(2, 2)
	b.Setup()
	b.Print()
	moves := b.Candidates(BLACK)
	fmt.Printf("len(moves) %d, moves = %v\n", len(moves), moves)
	// Output:
}
