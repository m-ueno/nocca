package nocca

const INFINITY = 100000

func flip(turn int) int {
	if turn == WHITE {
		return BLACK
	} else {
		return WHITE
	}
}

func MinMax(board *Board, turn, depthLimit int) (int, Move) {
	return minMax(board, turn, 0, depthLimit)
}

func minMax(board *Board, turn, depth, depthLimit int) (bestScore int, bestMove Move) {

	board.Print()

	if depth == depthLimit {
		bestScore = board.Evaluate()
		if turn == BLACK {
			bestScore = -bestScore
		}
		return
	}

	worklist := board.Candidates(turn)

	if len(worklist) == 0 {
		// 合法手がないので詰み
		bestScore = -INFINITY
		return
	}

	for _, move := range worklist {
		board.Apply(move)
		childScore, _ := minMax(board, flip(turn), depth+1, depthLimit)
		board.Back(move)

		if turn == WHITE {
			if childScore > bestScore {
				bestScore = childScore
				bestMove = move
			}
		} else {
			if childScore < bestScore {
				bestScore = childScore
				bestMove = move
			}
		}
	}
	return
}
