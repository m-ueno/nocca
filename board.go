package nocca

import (
	"fmt"
	"log"
	"os"
)

const (
	BLANK = iota
	WHITE
	BLACK
	FORBIDDEN
)

const (
	DEPTH  = 3
)

// type Piece struct {
// 	color int
// }

type Position struct {
	// x,yは1-origin, zは0-origin
	x, y, z int
}

type Move struct {
	from, to Position
}

type Board struct {
	width, height int
	board         []int
	turn          int
	topmost       [][]int // 画面座標で[x][y] 一番上のレイヤー番号 0,1,2
	moves         []Move
}

func NewBoard(width, height int) *Board {
	b := make([]int, (width+2)*(height+2)*DEPTH)

	topmost := make([][]int, width+1)
	for i := 0; i < len(topmost); i++ {
		topmost[i] = make([]int, height+1)
	}

	return &Board{
		width:   width,
		height:  height,
		board:   b,
		turn:    WHITE,
		topmost: topmost,
	}
}

func (b *Board) Setup() {
	for _, x := range []int{0, b.width + 1} {
		for y := 0; y < b.height+2; y++ {
			for z := 0; z < DEPTH; z++ {
				b.Set(x, y, z, FORBIDDEN)
			}
		}
	}
	for _, y := range []int{0, b.height + 1} {
		for x := 0; x < b.width+2; x++ {
			for z := 0; z < DEPTH; z++ {
				b.Set(x, y, z, FORBIDDEN)
			}
		}
	}
	for x := 1; x <= b.width; x++ {
		b.Set(x, 1, 0, BLACK)
		b.Set(x, b.height, 0, WHITE)
	}
}

// Get returns WHITE or BLACK at given position.
func (b *Board) Get(x, y, z int) int {
	w := b.width + 2
	h := b.height + 2
	idx := z*w*h + y*w + x
	if idx > (b.width+2)*(b.height+2)*DEPTH {
		log.Fatalf("idx %d is out of range x=%d y=%d z=%d", idx, x, y, z)
	}
	return b.board[idx]
}

func (b *Board) Set(x, y, z, new int) {
	w := b.width + 2
	h := b.height + 2
	idx := z*w*h + y*w + x
	if idx > (b.width+2)*(b.height+2)*DEPTH { // 56*3=168
		log.Fatalf("idx %d is out of range x=%d y=%d z=%d", idx, x, y, z)
	}
	b.board[idx] = new
}

func (b *Board) Print() {
	w := os.Stdout
	for y := 1; y <= b.height; y++ {
		fmt.Fprintf(w, "y=%d |", y)
		for x := 1; x <= b.width; x++ {
			for z := 0; z < 3; z++ {
				switch b.Get(x, y, z) {
				case WHITE:
					fmt.Fprintf(w, "白")
				case BLACK:
					fmt.Fprintf(w, "黒")
				default:
					fmt.Fprintf(w, "＿")
				}
			}
			fmt.Fprintf(w, "|")
		}
		fmt.Fprintf(w, "\n")
	}
	fmt.Fprintf(w, "\n")
}

// Evaluate returns score of white
func (b *Board) Evaluate() int {
	// 常に先手のスコアを計算
	var score int
	for x := 1; x <= b.width; x++ {
		for y := 1; y <= b.height; y++ {
			for z := 0; z < DEPTH; z++ {
				switch b.Get(x, y, z) {
				case WHITE:
					score += b.height - y + 1
				case BLACK:
					score -= y
				}
			}
		}
	}
	return score
}

func (b *Board) Candidates(turn int) []Move {
	var moves []Move
	for x := 1; x <= b.width; x++ {
		for y := 1; y <= b.height; y++ {
			z := b.topmost[x][y]
			if b.Get(x, y, z) != turn {
				continue
			}

			from := Position{x, y, z}

			for _, dx := range []int{-1, 0, 1} {
				for _, dy := range []int{-1, 0, 1} {
					if dx == 0 && dy == 0 {
						continue
					}
					if x+dx < 1 || x+dx > b.width || y+dy < 1 || y+dy > b.height {
						continue
					}
					if b.topmost[x+dx][y+dy] == 3 {
						continue
					}
					to := Position{x + dx, y + dy, b.topmost[x+dx][y+dy] + 1}
					moves = append(moves, Move{from: from, to: to})
				}
			}
		}
	}
	return moves
}

func (b *Board) Apply(m Move) {
	color := b.Get(m.from.x, m.from.y, m.from.z)
	b.Set(m.to.x, m.to.y, m.to.z, color)
	b.Set(m.from.x, m.from.y, m.from.z, 0)

	b.topmost[m.from.x][m.from.y] -= 1
	b.topmost[m.to.x][m.to.y] += 1
}

// Back applies reverse move
func (b *Board) Back(m Move) {
	mm := Move{
		from: m.to,
		to:   m.from,
	}
	b.Apply(mm)
}
