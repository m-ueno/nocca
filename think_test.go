package nocca

import (
	"fmt"
)

func ExampleTestMinMax() {
	b := NewBoard(2, 2)
	b.Setup()

	score, move := MinMax(b, WHITE, 1)

	fmt.Printf("score=%d, move=%v\n", score, move)
	// Output:

}
